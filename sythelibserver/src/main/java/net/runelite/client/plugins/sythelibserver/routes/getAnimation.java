package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.runelite.api.Client;
import net.runelite.client.plugins.Plugin;
import java.io.IOException;
import java.io.OutputStream;


public class getAnimation extends Plugin implements HttpHandler
{
	private Client client;
	public getAnimation(final Client client)
	{
		super();
		this.client = client;
	}

	public static int lastAnimationID;
	public static void setLastAnimationID(int animationID)
	{
		lastAnimationID = animationID;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		JsonObject object = new JsonObject();

		final int lastAnimation = lastAnimationID;
		object.addProperty("animationId", lastAnimation);
		Gson gson = new Gson();

		String respText = gson.toJson(object);
		exchange.sendResponseHeaders(200, respText.getBytes().length);
		OutputStream output = exchange.getResponseBody();
		output.write(respText.getBytes());
		output.flush();
		exchange.close();
	}

}

