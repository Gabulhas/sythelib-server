package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.Client;
import net.runelite.api.GameObject;
import net.runelite.api.ObjectDefinition;
import net.runelite.api.coords.WorldPoint;
import net.runelite.client.callback.ClientThread;
import javax.inject.Inject;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static net.runelite.client.plugins.sythelibserver.sythelibserverPlugin.loadedGameObjects;
import static net.runelite.client.plugins.sythelibserver.sythelibserverPlugin.objDefs;
import static net.runelite.client.plugins.sythelibserver.utils.routesUtils.queryToMap;


@Slf4j
public class getGameObjects implements HttpHandler
{
	public ObjectDefinition currentDef;
	private final Client client;
	public List<ObjectDefinition> ob = new ArrayList<>();

	@Inject
	private ClientThread clientThread;

	public getGameObjects(final Client client)
	{
		super();
		this.client = client;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{

		String OBJECTquery = "";
		if (exchange.getRequestURI().getQuery() != null)
		{
			Map<String, String> params = queryToMap(exchange.getRequestURI().getQuery());
			OBJECTquery = params.get("obj") != null ? params.get("obj") : "";
		}
		OBJECTquery = OBJECTquery.trim();
		log.info("QUERY: " + OBJECTquery);
		JsonObject mainObj = new JsonObject();
		JsonArray arr = new JsonArray();
		final Set<GameObject> objects = loadedGameObjects;

		int i = 0;

		for (GameObject go : objects)
		{
			JsonObject temp = new JsonObject();




			temp.addProperty("id", go.getId());

			if (!objDefs.containsKey(go.getId())) continue;

			String name = objDefs.get(go.getId());
			if (!OBJECTquery.equals("") && !OBJECTquery.equals(name)) continue;

			temp.addProperty("name", name);
			if (go.getWorldLocation() == null) continue;

			WorldPoint wp = go.getWorldLocation();
			temp.addProperty("x", wp.getX());
			temp.addProperty("y", wp.getY());

			if (go.getCanvasTilePoly() == null) continue;
			if (go.getCanvasTilePoly().getBounds() == null) continue;

			Rectangle clickbox = go.getCanvasTilePoly().getBounds();

			JsonObject canvas = new JsonObject();
			canvas.addProperty("x", clickbox.x);
			canvas.addProperty("y", clickbox.y);
			canvas.addProperty("height", clickbox.height);
			canvas.addProperty("width", clickbox.width);
			temp.add("canvas", canvas);

			arr.add(temp);
			i++;

		}
		mainObj.addProperty("total", i);
		mainObj.add("objects", arr);
		Gson gson = new Gson();
		String respText = gson.toJson(mainObj);
		exchange.sendResponseHeaders(200, respText.getBytes().length);
		OutputStream output = exchange.getResponseBody();
		output.write(respText.getBytes());
		output.flush();
		exchange.close();

	}

	public void getDefinition(int id)
	{
		currentDef = client.getObjectDefinition(id);
		log.info(currentDef.getName());
	}
}


