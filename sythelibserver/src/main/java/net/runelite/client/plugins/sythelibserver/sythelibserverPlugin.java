package net.runelite.client.plugins.sythelibserver;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Provides;
import com.sun.net.httpserver.HttpServer;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.Actor;
import net.runelite.api.ChatMessageType;
import net.runelite.api.Client;
import net.runelite.api.GameObject;
import net.runelite.api.GameState;
import net.runelite.api.InventoryID;
import net.runelite.api.Item;
import net.runelite.api.ItemContainer;
import net.runelite.api.ItemDefinition;
import net.runelite.api.NPC;
import net.runelite.api.ObjectDefinition;
import net.runelite.api.Player;
import net.runelite.api.events.GameObjectChanged;
import net.runelite.api.events.GameObjectDespawned;
import net.runelite.api.events.GameObjectSpawned;
import net.runelite.api.events.GameStateChanged;
import net.runelite.api.events.GameTick;
import net.runelite.api.events.ItemContainerChanged;
import net.runelite.api.events.NpcDespawned;
import net.runelite.api.events.NpcSpawned;
import net.runelite.api.events.PlayerDespawned;
import net.runelite.api.events.PlayerSpawned;
import net.runelite.client.callback.ClientThread;
import net.runelite.client.config.ConfigManager;
import net.runelite.client.eventbus.Subscribe;
import net.runelite.client.plugins.Plugin;
import net.runelite.client.plugins.PluginDescriptor;
import net.runelite.client.plugins.PluginType;
import net.runelite.client.plugins.sythelibserver.routes.getActors;
import net.runelite.client.plugins.sythelibserver.routes.getAnimation;
import net.runelite.client.plugins.sythelibserver.routes.getEquipment;
import net.runelite.client.plugins.sythelibserver.routes.getGameObjects;
import net.runelite.client.plugins.sythelibserver.routes.getInventory;
import net.runelite.client.plugins.sythelibserver.routes.getPlayers;
import net.runelite.client.plugins.sythelibserver.routes.getPos;
import net.runelite.client.plugins.sythelibserver.utils.objectJSON;
import org.pf4j.Extension;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import static net.runelite.client.plugins.sythelibserver.routes.getAnimation.setLastAnimationID;
import static net.runelite.client.plugins.sythelibserver.routes.getPos.setLc;

@Extension
@PluginDescriptor(
		name = "SytheLib Server Config",
		description = "Sythelibserver plugin.",
		type = PluginType.MISCELLANEOUS
)
@Slf4j
public class sythelibserverPlugin extends Plugin
{

	//API STUFF
	//NPCS
	public static Set<NPC> loadedNPCs = new HashSet<>();
	public static Set<Actor> loadedACTORs = new HashSet<>();

	//Players
	public static Set<Player> loadedPlayers = new HashSet<>();
	public static Set<Actor> loadedPlayersActors = new HashSet<>();

	//GameObjects
	public static Set<GameObject> loadedGameObjects = new HashSet<>();
	public static Set<ObjectDefinition> loadedGameObjectDefinitions;
	public static ObjectDefinition currentObj;
	public static Set<objectJSON> objectDefs = new HashSet<>();
	public static Map<Integer, String> objDefs = new HashMap<Integer, String>();

	//ItemContainers
	public static ItemContainer inventoryContainer;
	public static ArrayList<ItemDefinition> inventoryItemsDefinition = new ArrayList<>();
	public static ItemContainer equipmentContainer;
	public static ArrayList<ItemDefinition> equipmentItemsDefinition = new ArrayList<>();


	// Injects our config
	@Inject
	private sythelibserverConfig config;

	//injects client
	@Inject
	private Client client;


	public Gson gson = new Gson();

	@Inject
	private ClientThread clientThread;

	private HttpServer server;

	public Client getClient()
	{
		return this.client;
	}

	// Provides our config
	@Provides
	sythelibserverConfig provideConfig(ConfigManager configManager)
	{
		return configManager.getConfig(sythelibserverConfig.class);
	}

	@Override
	protected void startUp() throws IOException
	{
		// runs on plugin startup
		log.info("Plugin started");

		// example how to use config items
		// do stuff
		int port = config.portConfig();
		log.info("Port is " + port);
		currentObj = null;



		if (client.getGameState() == GameState.LOGGED_IN)
		{
			clientThread.invokeLater(() ->
			{
				client.addChatMessage(ChatMessageType.GAMEMESSAGE, "SytheLibServer", "Started on " + port, null);

					initializeVariables();
			});


		}
		try
		{
			String jsonObjs = readUrl("https://api.npoint.io/f0fed2a22b0a4717110f");

			Type objectListType = new TypeToken<Set<objectJSON>>(){}.getType();
			objectDefs = gson.fromJson(jsonObjs, objectListType);
			for (objectJSON obj : objectDefs)
			{
				objDefs.put(obj.getId(), obj.getName());
			}
			log.info(objDefs.size() + "");
			log.info(objDefs.get(1277) + " Obj");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		server = HttpServer.create(new InetSocketAddress(port), 0);

		server.createContext("/Actors", new getActors(this.client));
		server.createContext("/Inventory", new getInventory(this.client));
		server.createContext("/Animation", new getAnimation(this.client));
		server.createContext("/Pos", new getPos(this.client));
		server.createContext("/Equipment", new getEquipment(this.client));
		server.createContext("/Players", new getPlayers(this.client));
		server.createContext("/GameObjects", new getGameObjects(this.client));
		server.setExecutor(Executors.newSingleThreadExecutor());
		server.start();
	}

	@Override
	protected void shutDown()
	{
		// runs on plugin shutdown
		server.stop(1);
		loadedNPCs.clear();
		loadedACTORs.clear();
		log.info("Plugin stopped");
	}


	@Subscribe
	private void onGameStateChanged(GameStateChanged event)
	{
		if (event.getGameState() == GameState.LOGIN_SCREEN ||
				event.getGameState() == GameState.HOPPING)
		{
			inventoryItemsDefinition.clear();
			equipmentItemsDefinition.clear();
			loadedNPCs.clear();
			loadedACTORs.clear();
			loadedPlayersActors.clear();
			loadedPlayers.clear();
			loadedGameObjectDefinitions.clear();
			loadedGameObjects.clear();

			initializeVariables();
		}
	}

	@Subscribe
	private void onNpcSpawned(NpcSpawned npcSpawned)
	{
		if (npcSpawned.getNpc() != null)
		{
			final NPC npc = npcSpawned.getNpc();
			loadedNPCs.add(npc);
		}
		if (npcSpawned.getActor() != null)
		{
			final Actor actor = npcSpawned.getActor();
			loadedACTORs.add(actor);
		}
	}


	@Subscribe
	private void onNpcDespawned(NpcDespawned npcDespawned)
	{
		final NPC npc = npcDespawned.getNpc();
		final Actor actor = npcDespawned.getActor();
		if (loadedNPCs.contains(npc))
		{
			loadedNPCs.remove(npc);
		}
		if (loadedACTORs.contains(actor))
		{
			loadedACTORs.remove(actor);
		}

	}

	@Subscribe
	public void onPlayerSpawned(PlayerSpawned event)
	{

		final Player localPlayer = client.getLocalPlayer();


		if (event.getPlayer() != null && !event.getPlayer().equals(localPlayer))
		{
			final Player player = event.getPlayer();
			loadedPlayers.add(player);
		}
		if (event.getActor() != null && !event.getActor().getName().equals(localPlayer.getName()))
		{
			final Actor actor = event.getActor();
			loadedPlayersActors.add(actor);
		}

	}

	@Subscribe
	public void onPlayerDespawned(PlayerDespawned event)
	{
		loadedPlayers.remove(event.getPlayer());
		loadedPlayersActors.remove(event.getActor());
	}

	@Subscribe
	public void onItemContainerChanged(ItemContainerChanged event)
	{

		if (client.getItemContainer(InventoryID.INVENTORY).equals(event.getItemContainer()))
		{
			inventoryContainer = event.getItemContainer();
			inventoryItemsDefinition.clear();

			for (Item item : inventoryContainer.getItems())
			{
				inventoryItemsDefinition.add(client.getItemDefinition(item.getId()));
			}

		}
		else if (client.getItemContainer(InventoryID.EQUIPMENT).equals(event.getItemContainer()))
		{
			equipmentContainer = event.getItemContainer();
			equipmentItemsDefinition.clear();
			for (Item item : equipmentContainer.getItems())
			{
				equipmentItemsDefinition.add(client.getItemDefinition(item.getId()));
			}
		}
		InventoryID.
	}

	@Subscribe
	public void onGameTick(GameTick event)
	{
		Player LocalPlayer = client.getLocalPlayer();

		setLastAnimationID(LocalPlayer.getAnimation());
		setLc(LocalPlayer.getWorldLocation());
	}

	@Subscribe
	public void onGameObjectSpawned(GameObjectSpawned event)
	{

		final GameObject gameObject = event.getGameObject();
		if (gameObject != null)
		{
			loadedGameObjects.add(gameObject);
		}
	}

	@Subscribe
	public void onGameObjectChanged(GameObjectChanged event)
	{
		loadedGameObjects.remove(event.getPrevious());
		loadedGameObjects.add(event.getGameObject());
		log.info(event.getGameObject().toString());
	}

	@Subscribe
	public void onGameObjectDespawned(GameObjectDespawned event)
	{
		loadedGameObjects.remove(event.getGameObject());
	}

	public void initializeVariables()
	{

		setLastAnimationID(client.getLocalPlayer().getAnimation());

		inventoryContainer = client.getItemContainer(InventoryID.INVENTORY);
		assert inventoryContainer != null;
		for (Item item : inventoryContainer.getItems())
		{
			inventoryItemsDefinition.add(client.getItemDefinition(item.getId()));
		}

		equipmentContainer = client.getItemContainer(InventoryID.EQUIPMENT);
		assert equipmentContainer != null;
		for (Item item : equipmentContainer.getItems())
		{
			equipmentItemsDefinition.add(client.getItemDefinition(item.getId()));
		}

	}

	private static String readUrl(String urlString) throws Exception
	{
		BufferedReader reader = null;
		try
		{
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder buffer = new StringBuilder();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			log.info(buffer.toString());
			return buffer.toString();
		}
		finally
		{
			if (reader != null)
				reader.close();
		}
	}

}
