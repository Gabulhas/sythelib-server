package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.runelite.api.Client;
import net.runelite.api.coords.WorldPoint;
import java.io.IOException;
import java.io.OutputStream;

public class getPos implements HttpHandler
{
	public static WorldPoint lc;

	private Client client;
	public getPos(final Client client)
	{
		super();
		this.client = client;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{

		JsonObject obj = new JsonObject();
		obj.addProperty("x", lc.getX());
		obj.addProperty("y", lc.getY());

		Gson gson = new Gson();
		String respText = gson.toJson(obj);
		exchange.sendResponseHeaders(200, respText.getBytes().length);
		OutputStream output = exchange.getResponseBody();
		output.write(respText.getBytes());
		output.flush();
		exchange.close();
	}

	public static void setLc(WorldPoint lc)
	{
		getPos.lc = lc;
	}

}
