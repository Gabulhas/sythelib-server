package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.Client;
import net.runelite.api.Player;
import net.runelite.api.PlayerAppearance;
import net.runelite.api.coords.WorldPoint;
import net.runelite.api.kit.KitType;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;
import static net.runelite.client.plugins.sythelibserver.sythelibserverPlugin.loadedPlayers;


@Slf4j
public class getPlayers implements HttpHandler
{


	private final Client client;

	public getPlayers(final Client client)
	{
		super();
		this.client = client;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{

		try
		{
			final Set<Player> tempPlayers = loadedPlayers;

			Iterator it = tempPlayers.iterator();
			JsonObject mainObj = new JsonObject();
			JsonArray arr = new JsonArray();


			while (it.hasNext())
			{
				Player player = (Player) it.next();

				if (player == null) continue;

				String name = player.getName() == null ? "NULL" : player.getName();
				int x = -1;
				int y = -1;
				if (player.getWorldLocation() != null)
				{

					WorldPoint worldPoint = player.getWorldLocation();
					x = worldPoint.getX();
					y = worldPoint.getY();
				}
				int health = player.getHealthScale();
				int level = player.getCombatLevel();
				String overheadPrayer = player.getOverheadIcon() != null ? player.getOverheadText() : "None";
				int id = player.getPlayerId();
				boolean skulled = player.getSkullIcon() != null;

				JsonObject temp = new JsonObject();

				temp.addProperty("Name", name);
				temp.addProperty("id", id);
				temp.addProperty("x", x);
				temp.addProperty("y", y);
				temp.addProperty("health", health);
				temp.addProperty("level", level);
				temp.addProperty("Overhead prayer", overheadPrayer);
				temp.addProperty("isSkulled", skulled);
				if (player.getPlayerAppearance() != null)
				{
					PlayerAppearance ap = player.getPlayerAppearance();
					JsonObject equips = new JsonObject();

					//idk why there are errors here btw
					//I was using a for loop but idk

					equips.addProperty(KitType.HEAD.getName(), ap.getEquipmentId(KitType.HEAD));
					equips.addProperty(KitType.BOOTS.getName(), ap.getEquipmentId(KitType.BOOTS));
					equips.addProperty(KitType.CAPE.getName(), ap.getEquipmentId(KitType.CAPE));
					equips.addProperty(KitType.AMULET.getName(), ap.getEquipmentId(KitType.AMULET));
					equips.addProperty(KitType.HANDS.getName(), ap.getEquipmentId(KitType.HANDS));
					equips.addProperty(KitType.WEAPON.getName(), ap.getEquipmentId(KitType.WEAPON));
					equips.addProperty(KitType.TORSO.getName(), ap.getEquipmentId(KitType.TORSO));
					equips.addProperty(KitType.SHIELD.getName(), ap.getEquipmentId(KitType.SHIELD));
					equips.addProperty(KitType.LEGS.getName(), ap.getEquipmentId(KitType.LEGS));

					temp.add("equips", equips);
				}


				arr.add(temp);
				log.info((new Gson()).toJson(temp));

			}
			mainObj.add("players", arr);
			Gson gson = new Gson();
			String respText = gson.toJson(mainObj);
			exchange.sendResponseHeaders(200, respText.getBytes().length);
			OutputStream output = exchange.getResponseBody();
			output.write(respText.getBytes());
			output.flush();
			exchange.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(e.getLocalizedMessage() + " " + e.toString() + " " + e.getMessage() + e.getCause());
		}

	}
}
