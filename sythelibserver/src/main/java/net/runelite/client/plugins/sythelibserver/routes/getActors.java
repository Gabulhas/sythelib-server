package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.Actor;
import net.runelite.api.Client;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import static net.runelite.client.plugins.sythelibserver.sythelibserverPlugin.loadedACTORs;
import static net.runelite.client.plugins.sythelibserver.utils.routesUtils.queryToMap;


@Slf4j
public class getActors implements HttpHandler
{

	private final Client client;

	public getActors(final Client client)
	{
		super();
		this.client = client;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		final Set<Actor> tempList = loadedACTORs;
		String NPCquery = "";
		if (exchange.getRequestURI().getQuery() != null)
		{
			Map<String, String> params = queryToMap(exchange.getRequestURI().getQuery());
			NPCquery = params.get("NPC") != null ? params.get("NPC") : "";
		}
		String respText = "";
		Gson gson = new Gson();
		JsonArray arr = new JsonArray();
		log.info("Query " + NPCquery);
		try
		{

			Iterator it = tempList.iterator();
			while (it.hasNext())
			{
				Actor actor = (Actor) it.next();
				if (actor != null)
				{

					JsonObject obj = new JsonObject();
					String name = actor.getName();
					int combatLevel = actor.getCombatLevel();
					int hp = actor.getHealthScale();
					String interactingName = "";
					//location
					int x = -1;
					int y = -1;
					//canvas
					int xCanvas = -1;
					int yCanvas = -1;
					int heightCanvas = -1;
					int widthCanvas = -1;

					if (name == null)
					{
						name = "";
					}
					if (!NPCquery.equals(name) && !NPCquery.equals(""))
					{
						continue;
					}
					if (actor.getInteracting() != null)
					{
						if (actor.getInteracting().getName() != null)
						{
							interactingName = actor.getInteracting().getName();
						}
					}
					if (actor.getWorldLocation() != null)
					{
						x = actor.getWorldLocation().getX();
						y = actor.getWorldLocation().getY();
					}
					if (actor.getCanvasTilePoly() != null)
					{
						if (actor.getCanvasTilePoly().getBounds() != null)
						{
							Rectangle temp = actor.getCanvasTilePoly().getBounds();
							xCanvas = temp.x;
							yCanvas = temp.y;
							widthCanvas = temp.width;
							heightCanvas = temp.height;
						}
					}
					obj.addProperty("name", name);
					obj.addProperty("combatLevel", combatLevel);
					obj.addProperty("hp", hp);
					obj.addProperty("interacting", interactingName);
					obj.addProperty("x", x);
					obj.addProperty("y", y);
					JsonObject canvasObj = new JsonObject();
					canvasObj.addProperty("x", xCanvas);
					canvasObj.addProperty("y", yCanvas);
					canvasObj.addProperty("height", heightCanvas);
					canvasObj.addProperty("width", widthCanvas);


					obj.add("canvas", canvasObj);

					//obj.addProperty("",def.);
					arr.add(obj);
				}
			}
			JsonObject finalObj = new JsonObject();
			finalObj.add("npcs", arr);
			log.info(respText);
			respText = gson.toJson(finalObj);
		}
		catch (Exception e)
		{
			log.info(e.getMessage());
			log.info(e.getLocalizedMessage() + "");
			log.info(e.toString() + "");
		}

		log.info(respText);
		//Test
		exchange.sendResponseHeaders(200, respText.getBytes().length);
		OutputStream output = exchange.getResponseBody();
		output.write(respText.getBytes());
		output.flush();
		exchange.close();
	}
}
