package net.runelite.client.plugins.sythelibserver.routes;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.runelite.api.Client;
import net.runelite.api.Item;
import net.runelite.api.ItemContainer;
import net.runelite.api.ItemDefinition;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import static net.runelite.client.plugins.sythelibserver.sythelibserverPlugin.*;

public class getInventory implements HttpHandler
{

	private Client client;
	public static String inventoryAPIjson;
	public getInventory(final Client client)
	{
		super();
		this.client = client;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		try
		{

			JsonObject mainObj = new JsonObject();
			final ItemContainer container = inventoryContainer;
			final ArrayList<ItemDefinition> itemdefs = inventoryItemsDefinition;
			final Item[] itemArray = container.getItems();


			JsonArray arrayitems = new JsonArray();
			int totalItems = 0;
			for (int i = 0, itemArrayLength = itemArray.length; i < itemArrayLength; i++)
			{
				Item it = itemArray[i];
				if ( it == null) continue;
				ItemDefinition tempDef = itemdefs.get(i);
				if (tempDef == null || tempDef.getName().equals("null"))
				{
					continue;
				}

				JsonObject temp = new JsonObject();
				temp.addProperty("name", tempDef.getName());
				temp.addProperty("quantity", it.getQuantity());
				temp.addProperty("id", tempDef.getId());
				temp.addProperty("slot", i);
				arrayitems.add(temp);
				totalItems++;
			}
			mainObj.addProperty("slots", totalItems);
			mainObj.add("items", arrayitems);

			Gson gson = new Gson();

			String respText = gson.toJson(mainObj);
			exchange.sendResponseHeaders(200, respText.getBytes().length);
			OutputStream output = exchange.getResponseBody();
			output.write(respText.getBytes());
			output.flush();
			exchange.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public static void setInventoryAPIjson(String inventoryAPIjson)
	{
		getInventory.inventoryAPIjson = inventoryAPIjson;
	}
}